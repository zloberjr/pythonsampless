from Crypto.Cipher import AES

message = "this is unencrypted"
key1 = "firstkeyboiiiiii" #key must be 16, 24 or 32 bytes long
randomstr = "AAABBBCCCDDD"
iv1 = "thisiaiv10123456" #must be 16 bytes long
pre_enc1 = AES.new(key1, AES.MODE_CBC, iv1) #like sock = socket.socket...
print pre_enc1
print len(key1)
print len(iv1)
length = 16 - (len(message) % 16)
message += chr(length)*length
encrypted_message = pre_enc1.encrypt(message)
print encrypted_message
print "-" * 20
obj2 = AES.new(key1, AES.MODE_CBC, iv1)
decc = obj2.decrypt(encrypted_message)
print decc