import socket
import base64
from Crypto.Cipher import AES

udpip = "0.0.0.0"
udpport = 4441

print "==> Listen IP: ", udpip
print "==> Listen port: ", udpport
print "==> Creating Socket"

sockk = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sockk.bind((udpip, udpport))

def decry(encrypted_message):
	decc = base64.standard_b64decode(encrypted_message)
	return decc

def deAES(encrypted_message):
	key1 = "firstkeyboiiiiii" #key must be 16, 24 or 32 bytes long
	iv1 = "thisiaiv10123456" #must be 16 bytes long
	obj2 = AES.new(key1, AES.MODE_CBC, iv1)
	deccAES = obj2.decrypt(encrypted_message)
	return deccAES

print "==> Starting to listen..."
while True:
	encrypted_message, addr = sockk.recvfrom(1024) #buffer bytes
	print "==> Got: ", encrypted_message
	deAESS = deAES(encrypted_message)
	decc = decry(deAESS)
	print "==> Decoded: ", decc

