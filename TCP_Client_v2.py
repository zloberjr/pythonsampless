import socket

tcpip = "0.0.0.0"
tcpport = 4441
data = "ASDF movie"

print "==> Target IP: ", tcpip
print "==> Target port: ", tcpport
print "==> Creating Socket"
print "==> Data: ", data

sockk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sockk.connect((tcpip, tcpport))
print "==> Connected"
sockk.send(data)
response = sockk.recv(2048)
print "==> Response: ", response