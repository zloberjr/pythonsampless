import socket

udpip = "192.168.50.105"
udpport = 4441
PACKAGE = "a message that needs to be deliverddd AAAA"

print "==> Target IP: ", udpip
print "==> Target port: ", udpport
print "==> Message: ", PACKAGE
print "==> Creating Socket"

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
print "Sending message..."
sock.sendto(PACKAGE, (udpip, udpport))
print "==> Message sent"
