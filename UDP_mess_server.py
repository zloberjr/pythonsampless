import socket

udpip = "0.0.0.0"
udpport = 4441

print "==> Target IP: ", udpip
print "==> Target port: ", udpport
print "==> Creating Socket"

sockk = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sockk.bind((udpip, udpport))

print "==> Starting to listen..."
while True:
	data, addr = sockk.recvfrom(1024) #buffer bytes
	print "==> Got: ", data